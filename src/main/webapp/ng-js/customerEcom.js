app.controller('cusEComCtrl',function($scope,$http,$filter){
	
	
	 $scope.cid = window.location.hash;
	 var customerid =$scope.cid.split("/");
	 var cid = customerid[2];
	
	$scope.customerproduct=[];
	$scope.customercattle =[];
	
	var sortingOrder = 'name';
   	$scope.sortingOrder = sortingOrder;
    $scope.reverse = false;
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 12;
    $scope.pagedItems = [];
    $scope.currentPage = 0;
	
	$http.get('./rest/registration/getcustomerproduct').then(function(response){
	
		$scope.cusprdtdata = response.data;
		
		var searchMatch = function (haystack, needle) {
	        if (!needle) {
	            return true;
	        }
	        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	    };
	    
	    $scope.search = function () {
	        $scope.filteredItems = $filter('filter')($scope.cusprdtdata, function (item) {
	            for(var attr in item) {
	                if (searchMatch(item[attr], $scope.query))
	                    return true;
	            }
	            return false;
	        });
	        // take care of the sorting order
	        if ($scope.sortingOrder !== '') {
	            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	        }
	        $scope.currentPage = 0;
	        // now group by pages
	        $scope.groupToPages();
	    };
	    
	    
	    $scope.groupToPages = function () {
	        $scope.pagedItems = [];
	        
	        for (var i = 0; i < $scope.filteredItems.length; i++) {
	            if (i % $scope.itemsPerPage === 0) {
	                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
	            } else {
	                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
	            }
	        }
	    };
		
	    
	    $scope.range = function (start, end) {
	        var ret = [];
	        if (!end) {
	            end = start;
	            start = 0;
	        }
	        for (var i = start; i < end; i++) {
	            ret.push(i);
	        }
	        return ret;
	    };
	    
	    $scope.prevPage = function () {
	        if ($scope.currentPage > 0) {
	            $scope.currentPage--;
	        }
	    };
	    
	    $scope.nextPage = function () {
	        if ($scope.currentPage < $scope.pagedItems.length - 1) {
	            $scope.currentPage++;
	        }
	    };
	    
	    $scope.setPage = function () {
	        $scope.currentPage = this.n;
	    };

	    // functions have been describe process the data for display
	    $scope.search();
		
	});
	
	$scope.customerproduct.push($scope.cusprdtdata);
	
	//show and hide product view
	$scope.showitem = function() {
		$scope.viewallitem = true;
		$scope.viewproduct = false;
	}
	$scope.viewproduct = true;
	$scope.showproduct = function(){
		$scope.viewproduct = true;
		$scope.viewallitem = false;
	}
	//end
	
	
	$scope.total =0;
	$scope.cattletotal = 0;
	$scope.cart = [];
	$scope.tot=0;
	$scope.addItemToCart = function(product){
		
		$scope.tot++;
		if($scope.cart.length == 0){
			product.count = 1;
			$scope.cart.push(product);
		}else{
			var repeat = false;
			for(var i=0;i<$scope.cart.length;i++){
				if($scope.cart[i].product_id==product.product_id){
					repeat = true;
					$scope.cart[i].count +=1;
				}
			}
			if(!repeat){
				product.count = 1;
				$scope.cart.push(product);
			}
		}
		var expireDate = new Date();
	    expireDate.setDate(expireDate.getDate() + 1);
//		$cookies.putObject('cart', $scope.cart,  {'expires': expireDate});
//		$scope.cart = $cookies.getObject('cart');
			 
		$scope.total += parseFloat(product.product_offer);
	  //  $cookies.put('total', $scope.total,  {'expires': expireDate});
		
		/*$scope.cartTotal += product.product_offer;
	    $scope.cart.push(product);*/
	}
	
	$scope.removeItemCart = function(product){
		$scope.tot--;
		if(product.count > 1){
		     product.count -= 1;
		     var expireDate = new Date();
		     expireDate.setDate(expireDate.getDate() + 1);
		     //$cookies.putObject('cart', $scope.cart, {'expires': expireDate});
			 //  $scope.cart = $cookies.getObject('cart');
		   	}
		   	else if(product.count === 1){
		     var index = $scope.cart.indexOf(product);
			 $scope.cart.splice(index, 1);
			 expireDate = new Date();
			 expireDate.setDate(expireDate.getDate() + 1);
			// $cookies.putObject('cart', $scope.cart, {'expires': expireDate});
			// $scope.cart = $cookies.getObject('cart');
		     
		   }
		   
		   $scope.total -= parseFloat(product.product_offer);
		   //$cookies.put('total', $scope.total,  {'expires': expireDate});
		   $scope.cattletotal -= parseFloat(product.cattle_price);
		   
	};
	
		
	$http.get('./rest/registration/getcustomercattle').then(function(response){
		$scope.cuscatledata= response.data;
		
		var searchMatch = function (haystack, needle) {
	        if (!needle) {
	            return true;
	        }
	        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	    };
	    
	    $scope.search = function () {
	        $scope.filteredItems = $filter('filter')($scope.cuscatledata, function (item) {
	            for(var attr in item) {
	                if (searchMatch(item[attr], $scope.query))
	                    return true;
	            }
	            return false;
	        });
	        // take care of the sorting order
	        if ($scope.sortingOrder !== '') {
	            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	        }
	        $scope.currentPage = 0;
	        // now group by pages
	        $scope.groupToPages();
	    };
	    
	    
	    $scope.groupToPages = function () {
	        $scope.pagedItems = [];
	        
	        for (var i = 0; i < $scope.filteredItems.length; i++) {
	            if (i % $scope.itemsPerPage === 0) {
	                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
	            } else {
	                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
	            }
	        }
	    };
		
	    
	    $scope.range = function (start, end) {
	        var ret = [];
	        if (!end) {
	            end = start;
	            start = 0;
	        }
	        for (var i = start; i < end; i++) {
	            ret.push(i);
	        }
	        return ret;
	    };
	    
	    $scope.prevPage = function () {
	        if ($scope.currentPage > 0) {
	            $scope.currentPage--;
	        }
	    };
	    
	    $scope.nextPage = function () {
	        if ($scope.currentPage < $scope.pagedItems.length - 1) {
	            $scope.currentPage++;
	        }
	    };
	    
	    $scope.setPage = function () {
	        $scope.currentPage = this.n;
	    };

	    // functions have been describe process the data for display
	    $scope.search();
	    
	});
	
	$scope.customercattle.push($scope.cuscatledata);
	
	
	//For Food market
	
	$scope.viewprodlist = true;
	
	$scope.getviewprod= function(){
		$scope.viewselectedprod = true;
		$scope.viewprodlist = false;
	}
	
	$scope.showprodlist = function(){
		$scope.viewprodlist = true;
		$scope.viewselectedprod = false;
	}
	
	$scope.addToCart = function(product){
		$scope.tot++;
		if($scope.cart.length == 0){
			product.count = 1;
			$scope.cart.push(product);
		}else{
			var repeat = false;
			for(var i=0;i<$scope.cart.length;i++){
				if($scope.cart[i].product_id==product.product_id){
					repeat = true;
					$scope.cart[i].count +=1;
				}
			}
			if(!repeat){
				product.count = 1;
				$scope.cart.push(product);
			}
		}
		var expireDate = new Date();
	    expireDate.setDate(expireDate.getDate() + 1);
			 
		$scope.total += parseFloat(product.product_offer);
	}
	
	
	$scope.clkaddToCart = function(cattle){
		$scope.tot++;
		if($scope.cart.length == 0){
			cattle.count = 1;
			$scope.cart.push(cattle);
		}else{
			var repeat = false;
			for(var i=0;i<$scope.cart.length;i++){
				if($scope.cart[i].cattleid==cattle.cattleid){
					repeat = true;
					$scope.cart[i].count +=1;
				}
			}
			if(!repeat){
				cattle.count = 1;
				$scope.cart.push(cattle);
			}
		}
		var expireDate = new Date();
	    expireDate.setDate(expireDate.getDate() + 1);
			 
		$scope.cattletotal += parseFloat(cattle.cattle_price);
	}
	
});


