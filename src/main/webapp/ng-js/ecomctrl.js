app.controller('ecomctrl', function($scope,$http,$stateParams,$filter,$rootScope,$q) {
	
	
	   $scope.cid = window.location.hash;
	   var sellerid =$scope.cid.split("/");
	   var cid = sellerid[2];
	   var deferred = $q.defer();
	   
	   $http.get("./rest/ecomregister/getecomreglist/"+cid).then(function(response){
			$scope.getuser = response.data;
			$rootScope.sellerid = $scope.getuser[0].sellerid; 
	   });
	   
	   
	  $scope.getTotalPrice = function() {
		   var numVal1 = Number($scope.productrice || 0);
		   var numVal2 = Number($scope.productoffer) / 100 || 0;
		   $scope.producttotal = numVal1 - (numVal1 * numVal2);
		   
		   }
	   
	    $scope.productsadd =function(){
		   
		   var data ={
			  product_id:0,
		      product_name:$scope.productname,
	          product_category:$scope.productcategory,
	          product_price:$scope.productrice,
	          product_offer:$scope.productoffer,
	          product_image:$scope.productimage,
	          sellerid:cid
		   }
		   updateimg(data)
		 
		   }
	   	   
	   
	   
	   $scope.sendproductdata = function(data){
		      $http.post("./rest/ecomregister/ecomproductsave", data).then(function(response) {
		  		 $scope.productdata = response.data;
		  		$('#your-modal').modal('toggle');
		  	  },function(error){
		  		  consloe.log("sendproductdata ajax error" +error);
		  		  
		  	  });
		   }
	   
	   
	   
	   
	   var updateimg = function(data){
			var files = angular.element(document.querySelector('.imgupload'));
			if(files.val()!= 0){
				var formdata = new FormData();
			 	for(var i=0;i<files.length;i++){ 
			 		var fileObj= files[i].files;
			 		formdata.append("files" ,fileObj[0]);   
			    }
			 	
			 	 var xhr = new XMLHttpRequest();    
			 	 xhr.open("POST","./rest/file/upload/products");
			 	xhr.send(formdata);
			    	xhr.onload = function(e) {
			    		if (this.status == 200) {
			    			$scope.tmppath;
			    			var obj = JSON.parse(this.responseText);
			    			
			    			var imgpath = obj[0].path;
			    			
			    			data.product_image="https://s3.amazonaws.com/magazinebucket/Uploadimg/"+imgpath;
			    			
			    			if(data.product_id == 0){
			    				$scope.sendproductdata(data);
			    			}else{
			    				$scope.sendproductupdate(data);
			    			}
		    		    }
			    	};
			}else{
				$scope.sendproductupdate(data);
			}
		};
	   
		
		/*get the product process*/
		
		$scope.products= [];
		
		 $http.get("./rest/ecomregister/getproducts/"+cid).then(function(response){
				$scope.getproducts = response.data;
		   
				$scope.products.push($scope.getproducts);
		   },function(error){
			   console.log("get products ajax error" + error)
			   
		   });
		 
		 $scope.removeproducts = function(data){
		 
			 $http.post("./rest/ecomregister/productdelete/"+ data.product_id).then(function(response){
					$scope.producttrash = response.data;
					alert("Deleted the products successfully");
					window.location.reload();
				});
			}
		 
		 $scope.removecattle = function(data){
			 
			 $http.post("./rest/ecomregister/cattledelete/"+ data.cattleid).then(function(response){
					$scope.cattletrash = response.data;
					alert("Deleted the cattle successfully");
					window.location.reload();
				});
			}
		 
		 
		 $scope.Editproducts =function(data)
		 
		 {
		      $scope.productname=data.product_name;
	          $scope.productcategory = data.product_category;
	          $scope.productrice = data.product_price ;
	          $scope.productoffer = data.product_offer;
	          $scope.productimage = data.product_image ;
	          $scope.productid = data.product_id; 
	          $scope.selleridenty = data.sellerid ;
			 
		}
		 
		 $scope.sendproductupdate = function(data){
			   $http.post("./rest/ecomregister/productupdate",data).then(function(response){
					$scope.productupdate = response.data;
					
				}); 
		   }
		 
		 $scope.updateproducts =function(){
			 
			 var data = {
			 product_name:$scope.productname ,
			 product_category :$scope.productcategory ,
			 product_price :$scope.productrice ,
			 product_offer : $scope.productoffer ,
			 product_image : $scope.productimage ,
			 product_id : $scope.productid ,
			 product_id :  $scope.productid , 
			 sellerid : $scope.selleridenty
			 
			 }
			 updateimg(data)
		 }
		 
		 
		 
		 
		 
		 
		 $scope.productsupdate=true;
		 $scope.cattleupdate=false;
		 
		 $scope.productsupdates=function(){
	     $scope.productsupdate=true;
	     $scope.cattleupdate=false;
			  }
		 $scope.cattleupdates=function(){
		 
			 $scope.cattleupdate=true;
			 $scope.productsupdate=false;
			 
		 }
		 
		 $scope.cattleadd=function(){
			 var data ={
					  cattleid:0,
				      petname:$scope.cattlename,
			          pet_category:$scope.cattlecategory,
			          cattle_price:$scope.cattleprice,
			          cattle_image:$scope.cattleimage,
			          sellerid:cid
				 }
			 
			 updatecattleimg(data);
			 
		 }
		 
		 $scope.cattleedit=function(data){
			          
			          $scope.cattleids=data.cattleid;
					  $scope.cattlename= data.petname;
			          $scope.cattlecategory = data.pet_category;
			          $scope.cattleprice = data.cattle_price;
			          $scope.cattleimage = data.cattle_image;
			          $scope.selleridcattle= data.sellerid;
			          
		 }
		 $scope.cattleupdate=function(){
				var data={
			  cattleid:$scope.cattleids,
			  petname:$scope.cattlename,
			  pet_category:$scope.cattlecategory,
			  cattle_price:$scope.cattleprice,
			  cattle_image:$scope.cattleimage,
			  sellerid:$scope.selleridcattle
				}
				updatecattleimg(data);
}
		 
		 $scope.sendcattleupdatedata = function(data){
		      $http.post("./rest/ecomregister/cattleupdate", data).then(function(response) {
		  		 $scope.productdata = response.data;
		  		
		  	  },function(error){
		  		  consloe.log("sendcattledataupdate ajax error" +error);
		  		  
		  	  });
		   }
		 
		 
		 
		 
		 $scope.sendcattledata = function(data){
		      $http.post("./rest/ecomregister/ecomcattlesave", data).then(function(response) {
		  		 $scope.productdata = response.data;
		  		
		  	  },function(error){
		  		  consloe.log("sendcattledata ajax error" +error);
		  		  
		  	  });
		   }
		 
		 
		 var updatecattleimg = function(data){
				var files = angular.element(document.querySelector('.imgcattleupload'));
				if(files.val()!= 0){
					var formdata = new FormData();
				 	for(var i=0;i<files.length;i++){ 
				 		var fileObj= files[i].files;
				 		formdata.append("files" ,fileObj[0]);   
				    }
				 	
				 	 var xhr = new XMLHttpRequest();    
				 	 xhr.open("POST","./rest/file/upload/cattle");
				 	xhr.send(formdata);
				    	xhr.onload = function(e) {
				    		if (this.status == 200) {
				    			$scope.tmppath;
				    			var obj = JSON.parse(this.responseText);
				    			
				    			var imgpath = obj[0].path;
				    			
				    			data.cattle_image="https://s3.amazonaws.com/magazinebucket/Uploadimg/"+imgpath;
				    			
				    			if(data.cattleid == 0){
				    				$scope.sendcattledata(data);
				    			}else{
				    				$scope.sendcattleupdatedata(data);
				    			}
			    		    }
				    	};
				}else{
					$scope.sendcattleupdatedata(data);
				}
			};
		 
			
			$scope.cattle= [];
			
			 $http.get("./rest/ecomregister/getcattle/"+cid).then(function(response){
					$scope.getcattle = response.data;
			   
					$scope.cattle.push($scope.getcattle);
			   },function(error){
				   console.log("get cattle ajax error" + error);
				   
			   });
			 
			 $scope.foodmarket=true;
			 $scope.cattlemarket=false;
			 
			 $scope.openfoodmarket=function(){
				 
				 $scope.foodmarket=true;
				 $scope.cattlemarket=false;
				 
			 }		 
			 $scope.opencattlemarket = function(){
				 
				 $scope.foodmarket=false;
				 $scope.cattlemarket=true;
			 }

			  $scope.lowtohighprice="lowtohighprice"
			 
			 $scope.lowtohigh=function(){
				 if($scope.lowtohighprice === "lowtohighprice"){
				   
					 $scope.products[0]=$filter('orderBy')($scope.products[0],'product_price');
				 
				 }
	    }
			  
			  $scope.hightolowprice="hightolowprice"
					 
					 $scope.hightolow=function(){
						 if($scope.hightolowprice="hightolowprice"){
						   
							 $scope.products[0]=$filter('orderBy')($scope.products[0],'-product_price');
						 
						 }
			    }
		
		 
		 
			  $scope.cattlelowtohigh=function(){
					 
					   $scope.cattle[0]=$filter('orderBy')($scope.cattle[0],'cattle_price');
					 
						    }
			  $scope.cattlehightolow=function(){
					 
				  $scope.cattle[0]=$filter('orderBy')($scope.cattle[0],'-cattle_price');
					 
					}
		
		
	
	
});