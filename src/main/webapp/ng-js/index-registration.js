app.directive('validateEmail',function($http){
	return{
	restrict:'A',
	scope:true,
	require:'ngModel',
	link:function(scope,elem,attrs,ctrls){
		var ngModel=ctrls;
		scope.$watch(attrs.ngModel,function(email){
			 var url = './rest/ecomregister/getecomemail/'+email;
			 $http.get(url).then(function(data){
				 if(data.data[0].selleremail){
					 ngModel.$setValidity('validEmail',false);
				 }
				 else{
					 ngModel.$setValidity('validEmail',true);
				 }
			 });
		});
	}
	}
});

app.factory('authentication',function(){
	return{
		isAuthenticated:false,
		user:null
	}
});


/*app.directive('validateEcomLogin',function($http){
	return{
	restrict:'A',
	scope:true,
	require:'ngModel',
	link:function(scope,elem,attrs,ctrls){
		var ngModel=ctrls;
		scope.$watch(attrs.ngModel,function(email){
			 var url = './rest/ecomregister/getecomemail/'+email;
			 $http.get(url).then(function(data){
				 if(!data.data[0].selleremail){
			     ngModel.$setValidity('validecomloginusername',false);
				 }
				 
				 else if(!data.data[0].sellerpassword){
					 ngModel.$setValidity('validecomloginpassword',false);
				 }
				 else{
					 ngModel.$setValidity('validecomlogin',true);
				 }
			 });
		});
	}
	}
});*/




app.directive('validPasswordC', function() {
  return {
    require: 'ngModel',
    scope: {

      reference: '=validPasswordC'

    },
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue, $scope) {

        var noMatch = viewValue != scope.reference
        ctrl.$setValidity('noMatch', !noMatch);
        return (noMatch)?noMatch:!noMatch;
      });

      scope.$watch("reference", function(value) {;
        ctrl.$setValidity('noMatch', value === ctrl.$viewValue);

      });
    }
  }
});







app.controller('regCtrl', function($scope,$http,$stateParams,$filter,authentication,$timeout) {
	
	
	
	$scope.ph_numbr = /^\+?\d{10}$/;
    $scope.eml_add = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
	
	
	$scope.getuserreg=true;
	$scope.getdoctor=true;
	
	$(".radiobtnclk").click(function(){
		$scope.regrole = false;
	});
	
	$scope.regclk = function(){
		$scope.getuser;
		/*alert("Just checking--"+$scope.getuser);*/
		if($scope.getuser == "user"){
			window.location.href="#/userregistration";
			window.location.reload();
		}else if($scope.getuser == "doctor"){
			window.location.href="#/vetdocregistration";
			window.location.reload();
		}
		else if($scope.getuser == "supporter"){
			window.location.href="#/medicalsupporter";
			window.location.reload();
		}
		else if($scope.getuser == "seller"){
			window.location.href="#/ecomregistration";
			window.location.reload();
		}
		else{
			$scope.regrole = true;
		}
		
	}	
	/*$scope.regtype ="Please select the reg";*/
	
	$scope.changedValue = function(regtype){
		$scope.uname = "";
		$scope.pwd = "";
		if(regtype== "seller"){
			$http.get("./rest/ecomregister/typeofecomreg/"+regtype).then(function(response){
				$scope.gettypeofEcomreg = response.data;
				$scope.roleview = false;
			});
		}
		else if(regtype== "customer"){
			$http.get("./rest/registration/typeofreg/"+regtype).then(function(response){
				$scope.gettypeofreg = response.data;
				$scope.roleview = false;
			});
		}
		else if(regtype== "vetdoctor"){
			$http.get("./rest/registration/doctypeofreg/"+regtype).then(function(response){
				$scope.getdoctypeofreg = response.data;
				$scope.roleview = false;
			});
		}else if(regtype== "medical"){
			$http.get("./rest/medsupport/medreg/"+regtype).then(function(response){
				$scope.getmedtypeofreg = response.data;
				$scope.roleview = false;
			});
		}
	}
	
	$scope.prevlogin = function() {
		window.location.href="#/index-login";
	}
	
	
	//password visibility
	$('#password + .glyphicon').on('click', function() {
	   $(this).toggleClass('glyphicon-eye-close').toggleClass('glyphicon-eye-open'); // toggle our classes for the eye icon
	   $('#password').togglePassword(); // activate the hideShowPassword plugin
	});
	
	  
	/*$scope.params = {};
	  
	  $scope.showPassword = false;
	  
	  $scope.toggleShowPassword = function() {
	    $scope.showPassword = !$scope.showPassword;
	  }*/
	  
	
	$scope.loginclk = function(){
		$scope.loader =true;
		if($scope.loginform.$valid){
		var userrole = $('#userrole').val();
		
		if(userrole == null){
			//alert("please select the user");
			$scope.roleview = true;
		}else{
			angular.forEach($scope.gettypeofreg, function(value, key){
				$scope.bothview = false;
			    if(value.customer_email == $scope.uname){
			       	if(value.password == $scope.pwd){
			       		authentication.isAuthenticated =true; 
			       		$scope.loader =false;
			       		window.location.href="#/dashboard/"+value.customer_id;
			       	}
			    }
			    else{
					 $scope.bothview = false;
					 $timeout( function(){
						 $scope.bothview = true;
				     }, 1000 );
				}
			});
			
			angular.forEach($scope.gettypeofEcomreg, function(value, key){
				$scope.loader =true;
					 if(value.selleremail == $scope.uname){
						if(value.sellerpassword == $scope.pwd){
						  authentication.isAuthenticated =true;
						  $scope.loader =false;
		                  window.location.href="#/ecomdashboard/"+value.sellerid;
						}
					 }
					 else{
						 $scope.bothview = false;
						 $timeout( function(){
							 $scope.bothview = true;
					     }, 1000 );
				    }
	      });
			
		  angular.forEach($scope.getdoctypeofreg, function(value, key){
				$scope.loader =true;
		        	if(value.doctor_email == $scope.uname){
			        	if(value.password == $scope.pwd){
			        	 authentication.isAuthenticated =true; 
			        	 $scope.loader =false;
			        	 window.location.href="#/doctordashboard/"+value.doctorid;
			        	}
			         }
		        	else{
		        		$scope.bothview = false;
						 $timeout( function(){
							 $scope.bothview = true;
					     }, 1000 );
				    }
		  });	  
			
		  angular.forEach($scope.getmedtypeofreg, function(value, key){
				$scope.loader =true;
		        	if(value.med_email == $scope.uname){
			        	if(value.med_pwd == $scope.pwd){
			        	 authentication.isAuthenticated =true; 
			        	 $scope.loader =false;
			        	 window.location.href="#/meddashboard/"+value.med_id;
			        	}
			         }
		        	else{
		        		$scope.bothview = false;
						 $timeout( function(){
							 $scope.bothview = true;
					     }, 1000 );
				    }
		    });	  
		
		}
	  }
		$scope.loader =false;
	}
	
	
	
	$scope.userreg =function(){
		$scope.typeofreg = "customer";
	}
	
	$scope.saveuserreg = function(){
		 if($scope.customerform.$valid){
			$scope.userreg =function(){
				$scope.typeofreg = "customer";
			}
			var data = {
					customer_id:0,
					customer_name:$scope.customername,
					customer_email:$scope.email,
					customer_phone:$scope.contactno,
					customer_street:$scope.address,
					password:$scope.password,
					uid:$scope.uid,
					typeofreg:$scope.typeofreg,
					userphoto:$scope.userphoto,
					city:$scope.city
			}
			$scope.loadview = true;
			updateimg(data);
			
		 }
	}
    $scope.loadview = false;
	$scope.senduserimg = function(data){
		$http.post("./rest/registration/userregister",data).then(function(response) {
			$scope.userregistration = response.data;
			
			var someStr = $scope.userregistration
			var accept = someStr.replace(/['"]+/g, '')
			document.getElementById('load').style.visibility="visible";
			if(accept == "ACCEPTED"){
				$scope.loadview = false;
				$scope.successmsg = true;
				$timeout( function(){
					$scope.successmsg = false;
					window.location.href="#/index-login";
			    }, 4000 );
				 /*$scope.customername=""; $scope.uid="";
				 $scope.email="";
				 $scope.contactno="";
				 $scope.address="";
				 $scope.password="";
				 $scope.userphoto="";*/
			}

		})
	}
	
	var updateimg = function(data){
		var files = angular.element(document.querySelector('.imgupload'));
		if(files.val()!= 0){
			var formdata = new FormData();
		 	for(var i=0;i<files.length;i++){ 
		 		var fileObj= files[i].files;
		 		formdata.append("files" ,fileObj[0]);   
		    }
		 	
		 	 var xhr = new XMLHttpRequest();    
		 	 xhr.open("POST","./rest/file/upload/drproimg");
		 	xhr.send(formdata);
		    	xhr.onload = function(e) {
		    		if (this.status == 200) {
		    			//$scope.tmppath;
		    			var obj = JSON.parse(this.responseText);
		    			
		    			var imgpath = obj[0].path;
		    			
		    			data.userphoto = imgpath;
		    			
		    			if(data.customer_id == 0){
		    				$scope.senduserimg(data);
		    			}else{
		    				
		    			}
	    		    }
		    	};
		}else{
			$scope.senduserimg(data);
		}
	
	}
	//cancel user role
	$scope.rolecancel = function(){
		$scope.getuser ="";
	}
	
	//Image validation like jpg, png
	$("#user_profile_pic").change(function() {

	    var val = $(this).val();
	    switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
	        case 'gif': case 'jpg': case 'png':
	            alert("an image");
	            $scope.imgvalidate =  false;
	            break;
	        default:
	            $(this).val('');
	            // error message here
	            alert("please upload jpg and png image only");
	            $scope.imgvalidate =  true;
	            break;
	    }
	});
	
	$scope.ecomreg =function(){
		$scope.typeofreg = "seller";
	}
	
	
	
	$scope.saveecomreg = function(){
		var data = {
				sellername:$scope.ecomname,
				sellerstreet:$scope.ecomaddress,
				sellerphone:$scope.ecomcontactno,
				sellershop:$scope.ecomshopname,
				selleremail:$scope.ecomemail,
				sellerpassword:$scope.ecompassword,
				typeofreg:$scope.typeofreg
				}
		
		$http({
		    method: 'POST',
		    url: './rest/ecomregister/ecomregregister',
		    data: data,
		    headers: {
		        'Content-Type': 'application/json'
		    }}).then(function(response) {
		           console.log(response);
		           $scope.userregistration1 = response.data;
		           alert("sucessfully registered");
		           window.location.href="#/index-login";
		           
		       }, function(error) {
		           console.log(error);
		       });
		
		}
	

	
});