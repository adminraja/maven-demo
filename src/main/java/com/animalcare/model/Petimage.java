package com.animalcare.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="petimage")
public class Petimage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private int petimageid;
	private String pet_images;
	private String pet_videos;
	private String customer_id;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "appointmentid") 
	private Appointment appointment; 
	
	public int getPetimageid() {
		return petimageid;
	}
	public void setPetimageid(int petimageid) {
		this.petimageid = petimageid;
	}
	public String getPet_images() {
		return pet_images;
	}
	public void setPet_images(String pet_images) {
		this.pet_images = pet_images;
	}
		
	public String getPet_videos() {
		return pet_videos;
	}
	public void setPet_videos(String pet_videos) {
		this.pet_videos = pet_videos;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public Appointment getAppointment() {
		return appointment;
	}
	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}
	
	
	
}
