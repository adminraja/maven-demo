package com.animalcare.vo;

public class CustomerProductVO {

	private int customer_productid;
	private String product_category;
	private String product_name;
	private String price;
	private String product_description;
	private String customer_id;
	private String product_image;
	
	public int getCustomer_productid() {
		return customer_productid;
	}
	public void setCustomer_productid(int customer_productid) {
		this.customer_productid = customer_productid;
	}
	public String getProduct_category() {
		return product_category;
	}
	public void setProduct_category(String product_category) {
		this.product_category = product_category;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getProduct_description() {
		return product_description;
	}
	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getProduct_image() {
		return product_image;
	}
	public void setProduct_image(String product_image) {
		this.product_image = product_image;
	}
	
	
}
